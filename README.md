# Project: sumanie-cuisine

by: Iceyisaak

### This project features:

- Restaurant Website
- Sticky Navbar (initially at bottom, but sticks to top on scroll)
- Linear-Gradient
- Carousel Menu
- Reservation Form


### Technologies used
- Pure HTML/CSS (SASS)
- FontAwesome
- GoogleFonts

## Installation
1. `npm install` to create node_modules
2. `npm run start` to run the project on live-server
3. ENJOY!





